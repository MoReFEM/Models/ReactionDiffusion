if [[ -z "${MOREFEM_INSTALL_PREFIX}" ]]; then
    echo "Environment variable MOREFEM_INSTALL_PREFIX is not defined; please do so! (should point to the destination in which MoREFEM library was installed.)"
    exit 1
fi

if [[ -z "${EXTERNAL_MODEL_ROOT}" ]]; then
    echo "Environment variable EXTERNAL_MODEL_ROOT is not defined; please do so! (should point to the location where current model was cloned.)"
    exit 1
fi

python ${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/new_file.py --project=MoReFEM4ReactionDiffusion --sources_dir=${EXTERNAL_MODEL_ROOT}/Sources --copyright_notice=${MOREFEM_INSTALL_PREFIX}/share/CopyrightNotice.txt --project_namespace=MoReFEM::ReactionDiffusionNS
