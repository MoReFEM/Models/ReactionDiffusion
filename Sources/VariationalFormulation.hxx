// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

// > *** MoReFEM4ReactionDiffusion header guards *** //
#ifndef MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "VariationalFormulation.hpp"
// *** MoReFEM4ReactionDiffusion header guards *** < //

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/CourtemancheRamirezNattel.hpp" // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/FitzHughNagumo.hpp" // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hpp" // IWYU pragma: export

namespace MoReFEM::ReactionDiffusionNS
{


    template<Advanced::Concept::ReactionLaw ReactionLawT>
    VariationalFormulation<ReactionLawT>::VariationalFormulation(
        const NumberingSubset& numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data), numbering_subset_(numbering_subset)
    { }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::AllocateMatricesAndVectors()
    {
        const auto& numbering_subset = GetNumberingSubset();

        parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
        parent::AllocateSystemVector(numbering_subset);

        const auto& system_matrix = this->GetSystemMatrix(numbering_subset, numbering_subset);
        const auto& system_rhs = this->GetSystemRhs(numbering_subset);

        vector_current_horizontal_source_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_spiral_source_ = std::make_unique<GlobalVector>(system_rhs);
        matrix_capacity_per_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::SupplInit(const morefem_data_type& morefem_data)
    {
        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        using Diffusion = InputDataNS::Diffusion;
        diffusion_tensor_ =
            InitScalarParameterFromInputData<Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,
                                             time_manager_type>("Diffusion tensor", domain, morefem_data);
        density_ =
            InitScalarParameterFromInputData<Diffusion::Density, time_manager_type>("Density", domain, morefem_data);

        if (!GetDiffusionTensor().IsConstant())
            throw Exception("Current Reaction-Diffusion model is restricted to a "
                            "constant diffusion tensor.");

        if (!GetDiffusionDensity().IsConstant())
            throw Exception("Current Reaction-Diffusion  model is restricted to a "
                            "constant diffusion density.");

        using ReactionCoefficient = InputDataNS::ReactionNS::ReactionCoefficient;
        reaction_coefficient_ = InputDataNS::ExtractLeaf<ReactionCoefficient>(morefem_data);

        using RectangularSourceTimeParameter1 = InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::horizontal_source)>;

        initial_time_horizontal_source_ =
            InputDataNS::ExtractLeaf<RectangularSourceTimeParameter1::InitialTimeOfActivationList>(morefem_data);
        final_time_horizontal_source_ =
            InputDataNS::ExtractLeaf<RectangularSourceTimeParameter1::FinalTimeOfActivationList>(morefem_data);

        using RectangularSourceTimeParameter2 = InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::spiral_source)>;

        initial_time_spiral_source_ =
            InputDataNS::ExtractLeaf<RectangularSourceTimeParameter2::InitialTimeOfActivationList>(morefem_data);
        final_time_spiral_source_ =
            InputDataNS::ExtractLeaf<RectangularSourceTimeParameter2::FinalTimeOfActivationList>(morefem_data);

        using InitialConditionGate = InputDataNS::InitialConditionGate;

        do_write_gate_ = InputDataNS::ExtractLeaf<InitialConditionGate::WriteGate>(morefem_data);

        if (DoWriteGate())
        {
            using Result = InputDataNS::Result;

            decltype(auto) output_directory_path =
                InputDataNS::ExtractLeafAsPath<Result::OutputDirectory>(morefem_data);

            decltype(auto) mpi = this->GetMpi();

            FilesystemNS::Directory output_directory(mpi, output_directory_path, FilesystemNS::behaviour::ignore);

            gate_directory_ = std::make_unique<FilesystemNS::Directory>(output_directory, "Gate");
        }

        DefineOperators(morefem_data);
        PrepareDynamicRuns(morefem_data);
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::DefineOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = this->GetGodOfDof();

        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        const auto& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const auto& felt_space_horizontal_source =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::horizontal_source));
        const auto& felt_space_spiral_source = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::spiral_source));

        const auto& potential_ptr = UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::potential));

        namespace GVO = GlobalVariationalOperatorNS;

        conductivity_operator_ =
            std::make_unique<GVO::GradPhiGradPhi>(felt_space_highest_dimension, potential_ptr, potential_ptr);

        capacity_operator_ = std::make_unique<GVO::Mass>(felt_space_highest_dimension, potential_ptr, potential_ptr);

        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::horizontal_source)>;
            horizontal_source_parameter_ = InitScalarParameterFromInputData<parameter_type, time_manager_type>(
                "Horizontal source", domain, morefem_data);

            if (horizontal_source_parameter_ != nullptr)
            {
                horizontal_source_operator_ = std::make_unique<source_type>(
                    felt_space_horizontal_source, potential_ptr, *horizontal_source_parameter_);
            }
        }

        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::spiral_source)>;
            spiral_source_parameter_ = InitScalarParameterFromInputData<parameter_type, time_manager_type>(
                "Spiral source", domain, morefem_data);

            if (spiral_source_parameter_ != nullptr)
            {
                spiral_source_operator_ =
                    std::make_unique<source_type>(felt_space_spiral_source, potential_ptr, *spiral_source_parameter_);
            }
        }

        reaction_law_ = std::make_unique<reaction_law_type>(
            morefem_data, domain, felt_space_highest_dimension.GetQuadratureRulePerTopology());

        reaction_source_operator_ = std::make_unique<non_linear_source_operator_type>(
            felt_space_highest_dimension, potential_ptr, GetNonCstReactionLaw());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::PrepareDynamicRuns(const morefem_data_type& morefem_data)
    {
        AssembleStaticOperators();
        ComputeDynamicMatrices();

        const GodOfDof& god_of_dof = this->GetGodOfDof();

        const NumberingSubset& numbering_subset = GetNumberingSubset();
        const FEltSpace& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const Unknown& potential = UnknownManager::GetInstance().GetUnknown(AsUnknownId(UnknownIndex::potential));

        constexpr auto index = EnumUnderlyingType(InitialConditionIndex::potential_initial_condition);

        this->template SetInitialSystemSolution<index>(
            morefem_data, numbering_subset, potential, felt_space_highest_dimension);
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::AssembleStaticOperators()
    {
        const auto& numbering_subset = GetNumberingSubset();

        const double density = GetDiffusionDensity().GetConstantValue();

        auto& system_matrix = this->GetNonCstSystemMatrix(numbering_subset, numbering_subset);

        {
            const double diffusion_tensor = GetDiffusionTensor().GetConstantValue();
            GlobalMatrixWithCoefficient matrix(system_matrix, diffusion_tensor);
            GetConductivityOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }

        {
            const double capacity_coefficient = density / this->GetTimeManager().GetTimeStep();
            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixCapacityPerTimeStep(), capacity_coefficient);
            GetCapacityOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }

        const double reaction_coefficient = GetReactionCoefficient();

        if (horizontal_source_parameter_ != nullptr)
        {
            GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentHorizontalSource(), reaction_coefficient);
            GetHorizontalSourceOperator().Assemble(std::make_tuple(std::ref(vector)), 0.);
        }

        if (spiral_source_parameter_ != nullptr)
        {
            GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentSpiralSource(), reaction_coefficient);
            GetSpiralSourceOperator().Assemble(std::make_tuple(std::ref(vector)), 0.);
        }
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::ComputeDynamicMatrices()
    {
        const auto& numbering_subset = GetNumberingSubset();
        auto& system_matrix = this->GetNonCstSystemMatrix(numbering_subset, numbering_subset);
        const auto& capacity_matrix = GetMatrixCapacityPerTimeStep();

#ifndef NDEBUG
        AssertSameNumberingSubset(capacity_matrix, system_matrix);
#endif // NDEBUG

        Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., capacity_matrix, system_matrix);
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::AssembleNonLinearOperator()
    {
        const auto& numbering_subset = GetNumberingSubset();
        const auto& system_solution = this->GetSystemSolution(numbering_subset);

        auto& rhs = this->GetNonCstSystemRhs(GetNumberingSubset());

        rhs.ZeroEntries();

        using crn = Advanced::ReactionLawNS::CourtemancheRamirezNattel<time_manager_type>;

        if constexpr (std::is_same<ReactionLawT, crn>())
        {
            const double density = GetDiffusionDensity().GetConstantValue();
            // The ReactionCoefficient is not taking into account for this ionic model.
            GlobalVectorWithCoefficient source(rhs, -density);

            // Need the previous time step that is currently in system_solution.
            GetReactionSourceOperator().Assemble(std::make_tuple(std::ref(source)), system_solution);
        } else
        {
            GlobalVectorWithCoefficient source(rhs, GetReactionCoefficient());

            // Need the previous time step that is currently in system_solution.
            GetReactionSourceOperator().Assemble(std::make_tuple(std::ref(source)), system_solution);
        }
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::AssembleTransientOperator()
    {
        const double time = this->GetTimeManager().GetTime();
        auto& rhs = this->GetNonCstSystemRhs(GetNumberingSubset());

        if (horizontal_source_parameter_ != nullptr)
        {
            const double time_init = GetInitialTimeHorizontalSource();
            const double time_final = GetFinalTimeHorizontalSource();

            if (time > time_init && time < time_final)
                Wrappers::Petsc::AXPY(1., GetVectorCurrentHorizontalSource(), rhs);
        }

        if (spiral_source_parameter_ != nullptr)
        {
            const double time_init = GetInitialTimeSpiralSource();
            const double time_final = GetFinalTimeSpiralSource();

            if (time > time_init && time < time_final)
                Wrappers::Petsc::AXPY(1., GetVectorCurrentSpiralSource(), rhs);
        }
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::ComputeDynamicSystemRhs()
    {
        const auto& numbering_subset = GetNumberingSubset();

        this->GetNonCstSystemSolution(numbering_subset).UpdateGhosts();

        AssembleNonLinearOperator();
        AssembleTransientOperator();

        // Add the result of the previous step time : M*U_n.
        auto& rhs = this->GetNonCstSystemRhs(numbering_subset);
        Wrappers::Petsc::MatMultAdd(
            GetMatrixCapacityPerTimeStep(), this->GetSystemSolution(numbering_subset), rhs, rhs);
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation<ReactionLawT>::ImplementSnesFunction() const
    {
        return nullptr;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation<ReactionLawT>::ImplementSnesJacobian() const
    {
        return nullptr;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation<ReactionLawT>::ImplementSnesViewer() const
    {
        return nullptr;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation<ReactionLawT>::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation<ReactionLawT>::GetConductivityOperator() const noexcept
    {
        assert(!(!conductivity_operator_));
        return *conductivity_operator_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const typename VariationalFormulation<ReactionLawT>::source_type&
    VariationalFormulation<ReactionLawT>::GetHorizontalSourceOperator() const noexcept
    {
        assert(!(!horizontal_source_operator_));
        return *horizontal_source_operator_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const typename VariationalFormulation<ReactionLawT>::source_type&
    VariationalFormulation<ReactionLawT>::GetSpiralSourceOperator() const noexcept
    {
        assert(!(!spiral_source_operator_));
        return *spiral_source_operator_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline auto VariationalFormulation<ReactionLawT>::GetReactionSourceOperator() const noexcept
        -> const non_linear_source_operator_type&
    {
        assert(!(!reaction_source_operator_));
        return *reaction_source_operator_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalVariationalOperatorNS::Mass&
    VariationalFormulation<ReactionLawT>::GetCapacityOperator() const noexcept
    {
        assert(!(!capacity_operator_) && "Only exists in dynamic.");
        return *capacity_operator_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalMatrix& VariationalFormulation<ReactionLawT>::GetMatrixConductivity() const
    {
        assert(!(!matrix_conductivity_) && "This matrix is assembled directly in system_matrix.");
        return *matrix_conductivity_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline GlobalMatrix& VariationalFormulation<ReactionLawT>::GetNonCstMatrixConductivity()
    {
        return const_cast<GlobalMatrix&>(GetMatrixConductivity());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalMatrix& VariationalFormulation<ReactionLawT>::GetMatrixCapacityPerTimeStep() const
    {
        assert(!(!matrix_capacity_per_time_step_));
        return *matrix_capacity_per_time_step_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline GlobalMatrix& VariationalFormulation<ReactionLawT>::GetNonCstMatrixCapacityPerTimeStep()
    {
        return const_cast<GlobalMatrix&>(GetMatrixCapacityPerTimeStep());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalVector& VariationalFormulation<ReactionLawT>::GetVectorCurrentHorizontalSource() const
    {
        assert(!(!vector_current_horizontal_source_));
        return *vector_current_horizontal_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline GlobalVector& VariationalFormulation<ReactionLawT>::GetNonCstVectorCurrentHorizontalSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentHorizontalSource());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const GlobalVector& VariationalFormulation<ReactionLawT>::GetVectorCurrentSpiralSource() const
    {
        assert(!(!vector_current_spiral_source_));
        return *vector_current_spiral_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline GlobalVector& VariationalFormulation<ReactionLawT>::GetNonCstVectorCurrentSpiralSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentSpiralSource());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const NumberingSubset& VariationalFormulation<ReactionLawT>::GetNumberingSubset() const
    {
        return numbering_subset_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline auto VariationalFormulation<ReactionLawT>::GetDiffusionDensity() const -> const scalar_parameter_type&
    {
        assert(!(!density_));
        return *density_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline auto VariationalFormulation<ReactionLawT>::GetDiffusionTensor() const -> const scalar_parameter_type&
    {
        assert(!(!diffusion_tensor_));
        return *diffusion_tensor_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline auto VariationalFormulation<ReactionLawT>::GetReactionLaw() const -> const reaction_law_type&
    {
        assert(!(!reaction_law_));
        return *reaction_law_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline auto VariationalFormulation<ReactionLawT>::GetNonCstReactionLaw() -> reaction_law_type&
    {
        return const_cast<reaction_law_type&>(GetReactionLaw());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline double VariationalFormulation<ReactionLawT>::GetReactionCoefficient() const
    {
        return reaction_coefficient_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline bool VariationalFormulation<ReactionLawT>::DoWriteGate() const noexcept
    {
        return do_write_gate_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline double VariationalFormulation<ReactionLawT>::GetInitialTimeHorizontalSource() const
    {
        return initial_time_horizontal_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline double VariationalFormulation<ReactionLawT>::GetFinalTimeHorizontalSource() const
    {
        return final_time_horizontal_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline double VariationalFormulation<ReactionLawT>::GetInitialTimeSpiralSource() const
    {
        return initial_time_spiral_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline double VariationalFormulation<ReactionLawT>::GetFinalTimeSpiralSource() const
    {
        return final_time_spiral_source_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    const FilesystemNS::Directory& VariationalFormulation<ReactionLawT>::GetGateDirectory() const noexcept
    {
        assert(!gate_directory_);
        return *gate_directory_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void VariationalFormulation<ReactionLawT>::WriteGate() const
    {
        assert(DoWriteGate());

        const auto& mpi = this->GetMpi();

        std::ostringstream oconv;
        oconv << "gate_" << this->GetTimeManager().NtimeModified() << "_proc" << mpi.template GetRank<unsigned int>()
              << ".dat";

        auto filename = GetGateDirectory().AddFile(oconv.str());
        GetReactionLaw().WriteGate(filename);
    }

} // namespace MoReFEM::ReactionDiffusionNS

// > *** MoReFEM4ReactionDiffusion end header guards *** //
#endif // MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM4ReactionDiffusion end header guards *** < //
