// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

#define BOOST_TEST_MODULE model_reaction_diffusion
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

namespace // anonymous
{

    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension, std::string&& law, int Ntime_step);

    struct Fixture : public TestNS::FixtureNS::TestEnvironment
    {
        Fixture();
    };

} // namespace

PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(seq_2d_CRN, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Seq", "2D", "CRN", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(mpi4_2d_CRN, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Mpi4", "2D", "CRN", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(seq_2d_MS, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Seq", "2D", "MS", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(mpi4_2d_MS, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Mpi4", "2D", "MS", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(seq_1d_MS, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Seq", "1D", "MS", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(mpi4_1d_MS, Fixture)
{
    constexpr int Ntime_step{ 11 };
    CommonTestCase("Mpi4", "1D", "MS", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(seq_2d_FHN, Fixture)
{
    constexpr int Ntime_step{ 5 };
    CommonTestCase("Seq", "2D", "FHN", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(mpi4_2d_FHN, Fixture)
{
    constexpr int Ntime_step{ 5 };
    CommonTestCase("Mpi4", "2D", "FHN", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(seq_1d_FHN, Fixture)
{
    constexpr int Ntime_step{ 9 };
    CommonTestCase("Seq", "1D", "FHN", Ntime_step);
}

BOOST_FIXTURE_TEST_CASE(mpi4_1d_FHN, Fixture)
{
    constexpr int Ntime_step{ 9 };
    CommonTestCase("Mpi4", "1D", "FHN", Ntime_step);
}

PRAGMA_DIAGNOSTIC(pop)

namespace // anonymous
{

    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension, std::string&& law, int Ntime_step)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir, output_dir;

        (root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT"));
        (output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        std::ostringstream oconv;
        oconv << law << '/' << dimension;

        std::string ref_dir_path = root_dir + "/ExpectedResults/" + oconv.str();
        std::string obtained_dir_path = output_dir + std::string("/") + seq_or_par
                                        + std::string("/Ascii/ReactionDiffusion/") + oconv.str()
                                        + std::string("/Rank_0");

        FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory obtained_dir(obtained_dir_path, FilesystemNS::behaviour::read);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only
                                 // processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
        constexpr auto epsilon = 1.e-15; // lower epsilon yields a slight difference.

        for (auto i = 0; i < Ntime_step; ++i)
        {
            oconv.str("");
            oconv << "potential." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight> s(ref_dir, obtained_dir, oconv.str(), epsilon);
        }
    }

    Fixture::Fixture()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
            decltype(auto) cli_args = boost::unit_test::framework::master_test_suite().argv;

            environment.SetEnvironmentVariable(std::make_pair("EXTERNAL_MODEL_ROOT", cli_args[1]));

            first_call = false;
        }
    }

} // namespace
