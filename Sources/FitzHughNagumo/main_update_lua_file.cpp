// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //


#include "Model/Main/MainUpdateLuaFile.hpp"

#include "Model.hpp"


using namespace MoReFEM;

int main(int argc, char** argv)
{
    using model_type = ReactionDiffusionNS::Model<
    Advanced::ReactionLawNS::FitzHughNagumo<ReactionDiffusionNS::time_manager_type>>;
    
    return MoReFEM::ModelNS::MainUpdateLuaFile<model_type>(argc, argv);
}
