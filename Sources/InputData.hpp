// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

// > *** MoReFEM4ReactionDiffusion header guards *** //
#ifndef MOREFEM4REACTIONDIFFUSION_INPUTDATA_DOT_HPP_
#define MOREFEM4REACTIONDIFFUSION_INPUTDATA_DOT_HPP_
// *** MoReFEM4ReactionDiffusion header guards *** < //

#include "Utilities/Containers/EnumClass.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/RectangularSourceTimeParameter.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export

namespace MoReFEM::ReactionDiffusionNS
{


    enum class MeshIndex : std::size_t {
        mesh = 1 // only one mesh considered in current model!
    };

    enum class DomainIndex : std::size_t {
        highest_dimension = 1,
        horizontal_source = 2,
        spiral_source = 3,
        full_mesh = 4
    };

    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, horizontal_source = 2, spiral_source = 3 };

    enum class UnknownIndex : std::size_t {
        potential = 1,

    };

    enum class SolverIndex : std::size_t { solver = 1 };

    enum class NumberingSubsetIndex : std::size_t { monolithic = 1 };

    enum class ForceIndexList : std::size_t { horizontal_source = 1, spiral_source = 2 };

    enum class TensorIndex : std::size_t { diffusion_tensor = 1 };

    enum class InitialConditionIndex : std::size_t { potential_initial_condition = 1 };

    enum class RectangularSourceTimeParameterIndex : std::size_t { horizontal_source = 1, spiral_source = 2 };

    using InputDataTuple =
        std::tuple<InputDataNS::TimeManager,

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::horizontal_source),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::spiral_source),

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::mesh),

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::horizontal_source),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::spiral_source),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::horizontal_source),
                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::spiral_source),

                   MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(
                       InitialConditionIndex::potential_initial_condition),

                   InputDataNS::Diffusion::Density,
                   InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,

                   InputDataNS::ReactionNS::ReactionCoefficient,
                   InputDataNS::ReactionNS::FitzHughNagumo,
                   InputDataNS::ReactionNS::MitchellSchaeffer,

                   InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
                       RectangularSourceTimeParameterIndex::horizontal_source)>,
                   InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
                       RectangularSourceTimeParameterIndex::spiral_source)>,

                   InputDataNS::InitialConditionGate::Value,
                   InputDataNS::InitialConditionGate::WriteGate,

                   InputDataNS::Result>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
        using model_settings_tuple =
        std::tuple
        <
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::horizontal_source),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::spiral_source),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::monolithic),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::potential),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::horizontal_source),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::spiral_source),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),

            InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>::IndexedSectionDescription,

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::horizontal_source),
            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::spiral_source),

            MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::potential_initial_condition),
    
            InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(RectangularSourceTimeParameterIndex::horizontal_source)>::IndexedSectionDescription,
            InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(RectangularSourceTimeParameterIndex::spiral_source)>::IndexedSectionDescription
    
        >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    using input_data_type = InputData<InputDataTuple>;

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep<>>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::model>;

} // namespace MoReFEM::ReactionDiffusionNS

// > *** MoReFEM4ReactionDiffusion end header guards *** //
#endif // MOREFEM4REACTIONDIFFUSION_INPUTDATA_DOT_HPP_
// *** MoReFEM4ReactionDiffusion end header guards *** < //
