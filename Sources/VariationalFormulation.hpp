// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

// > *** MoReFEM4ReactionDiffusion header guards *** //
#ifndef MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM4ReactionDiffusion header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/NonLinearSource.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/VariationalFormulation.hpp"

#include "InputData.hpp"

namespace MoReFEM::ReactionDiffusionNS
{


    template<Advanced::Concept::ReactionLaw ReactionLawT>
    class VariationalFormulation final : public MoReFEM::VariationalFormulation<VariationalFormulation<ReactionLawT>,
                                                                                EnumUnderlyingType(SolverIndex::solver),
                                                                                time_manager_type>
    {
      private:
        //! Alias to the parent class.
        using parent = MoReFEM::
            VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver), time_manager_type>;

        //! Friendship to parent class, so this one can access private methods defined
        //! below through CRTP.
        friend parent;

        //! Alias to the non linear source type.
        using non_linear_source_operator_type = GlobalVariationalOperatorNS::NonLinearSource<ReactionLawT>;

        //! Alias to the reaction law type.
        using reaction_law_type = typename non_linear_source_operator_type::reaction_law_type;

        //! Alias to source type.
        using source_type = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar, time_manager_type>;

      public:
        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<time_manager_type>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<VariationalFormulation>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         */
        explicit VariationalFormulation(const NumberingSubset& numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        virtual ~VariationalFormulation() override = default;

        //! Copy constructor.
        VariationalFormulation(const VariationalFormulation&) = delete;

        //! Move constructor.
        VariationalFormulation(VariationalFormulation&&) = delete;

        //! Copy affectation.
        VariationalFormulation& operator=(const VariationalFormulation&) = delete;

        //! Move affectation.
        VariationalFormulation& operator=(VariationalFormulation&&) = delete;

        ///@}

        //! At each time iteration, compute the system Rhs.
        void ComputeDynamicSystemRhs();

        //! Boolean to decide to either create output of the Gate or not as it is the
        //! most consuming time part.
        bool DoWriteGate() const noexcept;

        //! Write the gate data into the appropriate file (depending on current time
        //! step).
        void WriteGate() const;

      private:
        /*!
         * \brief Prepare dynamic runs.
         *
         * For instance for dynamic iterations the system matrix is always the same;
         * compute it once and for all here.
         *
         * StaticOrDynamic rhs is what changes between two time iterations, but to
         * compute it the same matrices are used at each time iteration; they are also
         * computed there.
         */
        void PrepareDynamicRuns(const morefem_data_type& morefem_data);

        /*!
         * \brief Assemble method for all the static operatos (mass and stiffness).
         */
        void AssembleStaticOperators();

        /*!
         * \brief Assemble method for the nonlinear operator (source).
         */
        void AssembleNonLinearOperator();

        /*!
         * \brief Assemble method for the horizontal source operator.
         */
        void AssembleTransientOperator();

        //! Compute all the matrices required for dynamic calculation.
        void ComputeDynamicMatrices();

      public:
        /*!
         * \brief Get the only numbering subset relevant for this
         * VariationalFormulation.
         *
         * There is a more generic accessor in the base class but is use is more
         * unwieldy.
         */
        const NumberingSubset& GetNumberingSubset() const;

      private:
        /// \name CRTP-required methods.
        ///@{

        /*!
         * \brief Specific initialisation for derived class attributes.
         *
         * \internal <b><tt>[internal]</tt></b> This method is called by base class
         * method VariationalFormulation::Init().
         */
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to calculate the function required by
        //! the non-linear problem.
        Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;

        //! Define the pointer function required to calculate the jacobian required by
        //! the non-linear problem.
        Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;

        //! Define the pointer function required to view the results required by the
        //! non-linear problem.
        Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;

        //! Define the pointer function required to test the convergence required by
        //! the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}

      private:
        /*!
         * \brief Define the properties of all the global variational operators
         * involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        void DefineOperators(const morefem_data_type& morefem_data);

        //! Get the conductivity operator.
        const GlobalVariationalOperatorNS::GradPhiGradPhi& GetConductivityOperator() const noexcept;

        //! Get the capacity operator.
        const GlobalVariationalOperatorNS::Mass& GetCapacityOperator() const noexcept;

        //! Get the horizontal source operator.
        const source_type& GetHorizontalSourceOperator() const noexcept;

        //! Get the spiral source operator.
        const source_type& GetSpiralSourceOperator() const noexcept;

        //! Get the source operator.
        const non_linear_source_operator_type& GetReactionSourceOperator() const noexcept;

      private:
        /// \name Accessors to vectors and matrices specific to the reaction-diffusion
        /// problem.
        ///@{

        const GlobalMatrix& GetMatrixConductivity() const;

        GlobalMatrix& GetNonCstMatrixConductivity();

        const GlobalMatrix& GetMatrixCapacityPerTimeStep() const;

        GlobalMatrix& GetNonCstMatrixCapacityPerTimeStep();

        const GlobalVector& GetVectorCurrentHorizontalSource() const;

        GlobalVector& GetNonCstVectorCurrentHorizontalSource();

        const GlobalVector& GetVectorCurrentSpiralSource() const;

        GlobalVector& GetNonCstVectorCurrentSpiralSource();

        ///@}

        /// \name Material parameters.
        ///@{

        //! Diffusion density,
        const scalar_parameter_type& GetDiffusionDensity() const;

        //! Diffusion tensor.
        const scalar_parameter_type& GetDiffusionTensor() const;

        ///@}

        //! Constant access to reaction Law.
        const reaction_law_type& GetReactionLaw() const;

        //! Non constant access to reaction Law.
        reaction_law_type& GetNonCstReactionLaw();

        //! Reaction Coefficient
        double GetReactionCoefficient() const;

      private:
        //! Directory that stores Gate data.
        const FilesystemNS::Directory& GetGateDirectory() const noexcept;

      private:
        //! Acces to inital_time_horizontal_source.
        double GetInitialTimeHorizontalSource() const;

        //! Acces to final_time_horizontal_source.
        double GetFinalTimeHorizontalSource() const;

        //! Acces to inital_time_spiral_source.
        double GetInitialTimeSpiralSource() const;

        //! Acces to final_time_spiral_source.
        double GetFinalTimeSpiralSource() const;

      private:
        /// \name Global variational operators.
        ///@{

        //! Conductivity operator.
        GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr conductivity_operator_ = nullptr;

        //! Capacity operator.
        GlobalVariationalOperatorNS::Mass::const_unique_ptr capacity_operator_ = nullptr;

        //! Horizontal source operator.
        source_type::const_unique_ptr horizontal_source_operator_ = nullptr;

        //! Spiral source operator.
        source_type::const_unique_ptr spiral_source_operator_ = nullptr;

        //! Source operator.
        typename non_linear_source_operator_type::const_unique_ptr reaction_source_operator_ = nullptr;

        ///@}

        /// \name Parameters used to define TransientSource operators.
        ///@{

        //! Volumic source parameter.
        typename scalar_parameter_type::unique_ptr horizontal_source_parameter_ = nullptr;

        //! Spiral source parameter.
        typename scalar_parameter_type::unique_ptr spiral_source_parameter_ = nullptr;

        ///@}

      private:
        /// \name Global vectors and matrices specific to the reaction-diffusion
        /// problem.
        ///@{

        //! Matrix current conductivity.
        GlobalMatrix::unique_ptr matrix_conductivity_ = nullptr;

        //! Current capacity per time matrix.
        GlobalMatrix::unique_ptr matrix_capacity_per_time_step_ = nullptr;

        //! Current volumic source vector.
        GlobalVector::unique_ptr vector_current_horizontal_source_ = nullptr;

        //! Current spiral source vector.
        GlobalVector::unique_ptr vector_current_spiral_source_ = nullptr;

        ///@}

      private:
        //! \name Material parameters.
        ///@{

        //! Diffusion tensor.
        typename scalar_parameter_type::unique_ptr diffusion_tensor_ = nullptr;

        //! Density.
        typename scalar_parameter_type::unique_ptr density_ = nullptr;

        ///@}

        double reaction_coefficient_;

      private:
        //! Do Write Gate in file ?
        bool do_write_gate_ = false;

        //! Reaction law
        typename reaction_law_type::unique_ptr reaction_law_ = nullptr;

      private:
        //! Path to the directory that stores Gate data.
        FilesystemNS::Directory::const_unique_ptr gate_directory_ = nullptr;

      private:
        double initial_time_horizontal_source_;

        double final_time_horizontal_source_;

        double initial_time_spiral_source_;

        double final_time_spiral_source_;

      private:
        //! Numbering subset covering the displacement in the main finite element
        //! space.
        const NumberingSubset& numbering_subset_;
    };

} // namespace MoReFEM::ReactionDiffusionNS

#include "VariationalFormulation.hxx"

// > *** MoReFEM4ReactionDiffusion end header guards *** //
#endif // MOREFEM4REACTIONDIFFUSION_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM4ReactionDiffusion end header guards *** < //
