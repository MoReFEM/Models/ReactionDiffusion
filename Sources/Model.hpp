// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

// > *** MoReFEM4ReactionDiffusion header guards *** //
#ifndef MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HPP_
#define MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HPP_
// *** MoReFEM4ReactionDiffusion header guards *** < //

#include <memory>
#include <vector>

#include "Model/Model.hpp"

#include "InputData.hpp"
#include "VariationalFormulation.hpp"

namespace MoReFEM::ReactionDiffusionNS
{


    //! Model that implements a Reaction Diffusion. The time scheme used is an
    //! implicit-explicit one.
    template<Advanced::Concept::ReactionLaw ReactionLawT>
    class Model final
    : public MoReFEM::Model<Model<ReactionLawT>, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      public:
        //! Alias to self.
        using self = Model<ReactionLawT>;

        //! Return the name of the model.
        static const std::string& ClassName();

        //! Convenient alias to parent.
        using parent = MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        //! Friendship granted to the base class so this one can manipulates private
        //! methods.
        friend parent;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<Model<ReactionLawT>>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         */
        explicit Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! Copy constructor.
        Model(const Model&) = delete;

        //! Move constructor.
        Model(Model&&) = delete;

        //! Copy affectation.
        Model& operator=(const Model&) = delete;

        //! Move affectation.
        Model& operator=(Model&&) = delete;

        ///@}

        /// \name Crtp-required methods.
        ///@{

        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();

        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();

        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize() noexcept;

      private:
        //! Non constant access to the underlying VariationalFormulation object.
        VariationalFormulation<ReactionLawT>& GetNonCstVariationalFormulation() noexcept;

        //! Access to the underlying VariationalFormulation object.
        const VariationalFormulation<ReactionLawT>& GetVariationalFormulation() const noexcept;

      private:
        /*!
         * \brief Whether the model wants to add additional cases in which the Model
         * stops (besides the reach of maximum time).
         *
         * Returns always true (no such additional condition in this Model).
         */
        bool SupplHasFinishedConditions() const noexcept;

        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep() noexcept;

        ///@}

      private:
        //! Underlying variational formulation.
        typename VariationalFormulation<ReactionLawT>::unique_ptr variational_formulation_;
    };

} // namespace MoReFEM::ReactionDiffusionNS

#include "Model.hxx"

// > *** MoReFEM4ReactionDiffusion end header guards *** //
#endif // MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HPP_
// *** MoReFEM4ReactionDiffusion end header guards *** < //
