// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

// > *** MoReFEM4ReactionDiffusion header guards *** //
#ifndef MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HXX_
#define MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HXX_
// IWYU pragma: private, include "Model.hpp"
// *** MoReFEM4ReactionDiffusion header guards *** < //

namespace MoReFEM::ReactionDiffusionNS
{


    template<Advanced::Concept::ReactionLaw ReactionLawT>
    Model<ReactionLawT>::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void Model<ReactionLawT>::SupplInitialize()
    {
        const auto& god_of_dof = this->GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();
        const auto& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        {
            variational_formulation_ = std::make_unique<VariationalFormulation<ReactionLawT>>(
                numbering_subset, god_of_dof, DirichletBoundaryCondition::vector_shared_ptr(), morefem_data);
        }

        auto& formulation = this->GetNonCstVariationalFormulation();

        formulation.Init(morefem_data);
        formulation.WriteSolution(this->GetTimeManager(), numbering_subset);

        if (formulation.DoWriteGate())
            formulation.WriteGate();

        const PetscReal buf_min = formulation.GetSystemSolution(numbering_subset).Min().second;
        const PetscReal buf_max = formulation.GetSystemSolution(numbering_subset).Max().second;

        if (this->GetMpi().IsRootProcessor())
        {
            std::cout << std::endl;
            std::cout << "Minimum potential : " << buf_min << std::endl;
            std::cout << "Maximum potential : " << buf_max << std::endl;
        }
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void Model<ReactionLawT>::Forward()
    {
        auto& formulation = this->GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the
        // system.
        formulation.ComputeDynamicSystemRhs();

        const auto& numbering_subset = formulation.GetNumberingSubset();

        if (this->GetTimeManager().NtimeModified() == 1)
        {
            formulation.template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                numbering_subset, numbering_subset);
            formulation.template SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset);
        } else
        {
            formulation.template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(
                numbering_subset, numbering_subset);
            formulation.template SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset);
        }
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void Model<ReactionLawT>::SupplFinalizeStep()
    {
        auto& formulation = this->GetNonCstVariationalFormulation();
        const auto& numbering_subset = formulation.GetNumberingSubset();

        if ((this->GetTimeManager().NtimeModified() % this->GetDisplayValue()) == 0)
        {
            formulation.WriteSolution(this->GetTimeManager(), numbering_subset);

            const PetscReal buf_min = formulation.GetSystemSolution(numbering_subset).Min().second;
            const PetscReal buf_max = formulation.GetSystemSolution(numbering_subset).Max().second;

            if (this->GetMpi().IsRootProcessor())
            {
                std::cout << "Minimum potential : " << buf_min << std::endl;
                std::cout << "Maximum potential : " << buf_max << std::endl;
            }
        }

        if (formulation.DoWriteGate())
            formulation.WriteGate();
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    void Model<ReactionLawT>::SupplFinalize() noexcept
    { }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const std::string& Model<ReactionLawT>::ClassName()
    {
        static std::string name("Model");
        return name;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline const VariationalFormulation<ReactionLawT>& Model<ReactionLawT>::GetVariationalFormulation() const noexcept
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline VariationalFormulation<ReactionLawT>& Model<ReactionLawT>::GetNonCstVariationalFormulation() noexcept
    {
        return const_cast<VariationalFormulation<ReactionLawT>&>(GetVariationalFormulation());
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline bool Model<ReactionLawT>::SupplHasFinishedConditions() const noexcept
    {
        return false; // ie no additional condition
    }

    template<Advanced::Concept::ReactionLaw ReactionLawT>
    inline void Model<ReactionLawT>::SupplInitializeStep() noexcept
    { }

} // namespace MoReFEM::ReactionDiffusionNS

// > *** MoReFEM4ReactionDiffusion end header guards *** //
#endif // MOREFEM4REACTIONDIFFUSION_MODEL_DOT_HXX_
// *** MoReFEM4ReactionDiffusion end header guards *** < //
