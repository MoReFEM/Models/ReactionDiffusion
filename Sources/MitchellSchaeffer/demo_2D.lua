-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 1

} -- transient

-- Finite element space for highest geometric dimension
-- unknown_list: { 'potential' }
FiniteElementSpace1 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace1

-- Finite element space for Neumann boundary condition
-- unknown_list: { 'potential' }
FiniteElementSpace2 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace2

-- Finite element space for Robin boundary condition
-- unknown_list: { 'potential' }
FiniteElementSpace3 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace3

-- Sole mesh
Mesh1 = {

	-- Path of the mesh file to use. 
	-- IMPORTANT: if run from preprocessed data, this path won't be used (the already cut mesh will be used 
	-- instead). 
	-- Expected format: "VALUE"
	mesh = "${EXTERNAL_MODEL_ROOT}/Data/Mesh/reaction_diffusion_2D.mesh",

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = 'Medit',

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 2,

	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1

} -- Mesh1

-- Highest dimension geometric elements
-- mesh_index: { 1 }
Domain1 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain1

-- Horizontal source
-- mesh_index: { 1 }
Domain2 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain2

-- Spiral source
-- mesh_index: { 1 }
Domain3 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain3

-- Covers the whole mesh
-- mesh_index: { 1 }
Domain4 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = {  },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain4

-- Solver
Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-09,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-08,

	-- Solver to use. The list includes all the solvers which have been explicitly activated in MoReFEM. 
	-- However, it does not mean all are actually available in your local environment: some require that some 
	-- external dependencies were added along with PETSc. The ThirdPartyCompilationFactory facility set them up 
	-- by default. 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Gmres', 'Mumps', 'Petsc', 'SuperLU_dist', 'Umfpack' })
	solver = 'SuperLU_dist'

} -- Petsc1

-- Horizontal source
ScalarTransientSource1 = {

	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = 'constant',

	-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: see the variant description...
	value = 0.05

} -- ScalarTransientSource1

-- Spiral source
ScalarTransientSource2 = {

	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = 'lua_function',

	-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: see the variant description...
	value = 
[[
    function (x, y, z)
        if (((x>=6.) and (x<=9.9)) and (y>=5.)) then
            return 0.01;
        else
            return 0.;
        end
    end 
    ]]

} -- ScalarTransientSource2

-- Initial condition for potential
InitialCondition1 = {

	-- How is given the parameter (as a constant, per quadrature point, etc...). Choose "ignore" if you do not 
	-- want this parameter (in this case it will stay at nullptr) - in this case all components must be 
	-- 'ignore'. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = { 'constant', 'constant', 'constant' },

	-- For each 'VALUE*n* (see 'Expected format' below), the format may be:  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { -80, 0, 0 }

} -- InitialCondition1

Diffusion = {

	Density = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 1

	}, -- Density

	Tensor1 = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 0.015

	}, -- Tensor1

} -- Diffusion

ReactionFitzHughNagumo = {

	-- Value of a in FitzHughNagumo.
	-- Expected format: VALUE
	a = 0.08,

	-- Value of b in FitzHughNagumo.
	-- Expected format: VALUE
	b = 0.7,

	-- Value of c in FitzHughNagumo.
	-- Expected format: VALUE
	c = 0.8

} -- ReactionFitzHughNagumo

ReactionMitchellSchaeffer = {

	tau_close = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 100

	}, -- tau_close

	-- Value of Tau_in in MitchellSchaeffer.
	-- Expected format: VALUE
	tau_in = 4,

	-- Value of Tau_out in MitchellSchaeffer.
	-- Expected format: VALUE
	tau_out = 90,

	-- Value of Tau_open in MitchellSchaeffer.
	-- Expected format: VALUE
	tau_open = 100,

	-- Value of U_gate in MitchellSchaeffer.
	-- Expected format: VALUE
	u_gate = -67,

	-- Value of U_min in MitchellSchaeffer.
	-- Expected format: VALUE
	u_min = -80,

	-- Value of U_max in MitchellSchaeffer.
	-- Expected format: VALUE
	u_max = 20

} -- ReactionMitchellSchaeffer

-- Horizontal source time parameter
RectangularSourceTimeParameter1 = {

	-- Value of the initial time to activate the source.
	-- Expected format: VALUE
	initial_time_of_activation = 0,

	-- Value of the final time to activate the source.
	-- Expected format: VALUE
	final_time_of_activation = 2.5

} -- RectangularSourceTimeParameter1

-- Spiral source time parameter
RectangularSourceTimeParameter2 = {

	-- Value of the initial time to activate the source.
	-- Expected format: VALUE
	initial_time_of_activation = 300,

	-- Value of the final time to activate the source.
	-- Expected format: VALUE
	final_time_of_activation = 305

} -- RectangularSourceTimeParameter2

InitialConditionGate = {

	-- Value of Initial Condition Gate in ReactionDiffusion.
	-- Expected format: VALUE
	initial_condition_gate = 0.0001,

	-- Either Write the Gate at each time step or not.
	-- Expected format: 'true' or 'false' (without the quote)
	write_gate = false

} -- InitialConditionGate

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_RESULT_DIR}/ReactionDiffusion/MS/2D",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,

	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

-- Value of R.
-- Expected format: VALUE
ReactionCoefficient = 1000

