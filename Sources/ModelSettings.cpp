// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

#include "InputData.hpp"

namespace MoReFEM::ReactionDiffusionNS
{

    void ModelSettings::Init()
    {
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
                "Monolithic numbering subset");
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>>({ "Potential" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>::Name>("potential");

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>::Nature>("scalar");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
                { "Highest dimension geometric elements" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::horizontal_source)>>(
                { "Horizontal source" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::spiral_source)>>({ "Spiral source" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
                { "Covers the whole mesh" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::horizontal_source)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::spiral_source)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
                { "Finite element space for highest geometric dimension" });
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::horizontal_source)>>(
                { "Finite element space for Neumann boundary condition" });
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::spiral_source)>>(
                { "Finite element space for Robin boundary condition" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
                { "potential" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::horizontal_source)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::horizontal_source)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::horizontal_source));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::horizontal_source)>::UnknownList>(
                { "potential" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::horizontal_source)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::spiral_source)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::spiral_source)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::spiral_source));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::spiral_source)>::UnknownList>(
                { "potential" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::spiral_source)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });
        }

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole mesh" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });

        SetDescription<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>>(
            { "Diffusion tensor" });

        SetDescription<InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::horizontal_source)>>(
            { "Horizontal source" });
        SetDescription<InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::spiral_source)>>(
            { "Spiral source" });

        SetDescription<
            InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::potential_initial_condition)>>(
            { "Initial condition for potential" });

        SetDescription<InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::horizontal_source)>>("Horizontal source time parameter");
        SetDescription<InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::spiral_source)>>("Spiral source time parameter");
    }

} // namespace MoReFEM::ReactionDiffusionNS
