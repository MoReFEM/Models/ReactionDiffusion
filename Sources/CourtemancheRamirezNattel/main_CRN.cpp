// > *** MoReFEM4ReactionDiffusion copyright notice *** //
/*!
 * This file is a reaction-diffusion model using
 * [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library. It is
 * released under the LGPL v3 license; a copy is provided in the LICENSE file
 * shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM
 * team-project](https://www.inria.fr/m3disim). You may contact its developers
 * by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4ReactionDiffusion was developed by Gautier Bureau, former developer
 * in M3DISIM team. It is loosely maintained by Sébastien Gilles
 * (sebastien.gilles@inria.fr), who does minimal updates to keep it working with
 * latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4ReactionDiffusion copyright notice *** < //

#include "Model/Main/Main.hpp"

#include "Model.hpp"

using namespace MoReFEM;

int main(int argc, char** argv)
{
    using model_type = ReactionDiffusionNS::Model<
        MoReFEM::Advanced::ReactionLawNS::CourtemancheRamirezNattel<ReactionDiffusionNS::time_manager_type>>;

    return ModelNS::Main<model_type>(argc, argv);
}
