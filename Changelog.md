# v24.42

- Update to work with MoReFEM v24.42.

- Set the project so that it might use some of the CoreLibrary scripts (to create a new file, update the specific parts such as header and banner at the top of the file, run clang-format, etc...)
- Refactor CMake so that it uses up the custom macro defined in CoreLibrary.
- Remove XCode projects, and ensure generation of an XCode project with CMake works.
- Refresh CI.


# v24.06

- Update to MoReFEM v24.06

Note: a v24.04 was emitted but has been removed: MoReFEM v24.04 contained a new bug related to invariants computation.


# v23.12

- Update to work with MoReFEM v23.11.2.


# v22.37

- Update to MoReFEM v22.37.

# v21.28

- Update to MoReFEM v21.27.
- Fix XCode build (requires this [MoReFEM ticket](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1663) not yet in MoReFEM v21.27).

# v21.23

- Update to MoReFEM v21.22.
- CI: remove tags for Docker builds (as shared runners are now used).

# v21.07

- The code didn't need any modification to run against MoReFEM v21.06.

# v21.01

- Upgrade to MoReFEM v20.52
  . Reference Ensight mesh file have been modified to reflect the change of convention used (node id given rather than assign).
- Add new Gitlab feature 'needs' in Yaml.
- Add override keyword to fix new clang warning.

# v20.18

- Update to new MoReFEM API: modify the expected 'interfaces.hhdata' files so that they follow new convention, and check them only in sequential mode.

# v20.12

- Remove DebugPrintNorm() call, which was removed in MoReFEM core library.
- Modify the registry from which the MoReFEM base images should be fetched (until MoReFEM #1509 is resolved)


# v19.52:

- Feature #1 Introduce CI.

# v19.47:

- Update to the new MoReFEM API

# v19.27.2:

- Update to the new MoReFEM API (especially Lua files)

# v19.16:

- Update to the new MoReFEM API (especially tests now handled with Boost.Test).
- Fix the README.


# v18.47:

- MoReFEM Support #1374: Replace MOREFEM_ROOT by more accurate MODEL_ROOT_DIRECTORY.
- MoReFEM Support #1373: Update the model to the v18.47 MoReFEM API.
- **MoReFEM Support #1371**: Propagate this ticket from MoReFEM library that rename the InputParameterList InputData.

# v18.32:

- MoReFEM Feature #1322: Add integration test.

# v18.30:

- MoReFEM Support #1309: Update the Lua files to the simplified new interface for parameters.

# v18.27:

- Make it compliant with MoReFEM v18.27.


# v18.12.2

- MoReFEM Support #1256: Update model to work with MoReFEM v18.12.2. SCons build has been replaced by CMake one.
