A reaction diffusion model with 3 reaction laws.

This model has been successfully run with the MoReFEM release v24.41.


# Installation

## CMake / Ninja (or make) build

- Set properly `MOREFEM_INSTALL_PREFIX` to point to the directory in which MoReFEM Core library we
installed (through a command such as `ninja install` that must have been called).

- Create a `build` (or whatever you want to name it) directory, got inside and type:

```shell
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Ninja"
```

which calls CMake with some adequate configuration options.

Then compile with 

```shell
ninja
```

## XCode

If you're on a mac and want to use a XCode project to compile and run the code, generate the XCode projet through:


```shell
mkdir XCode && cd XCode
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Xcode"
```

(see above for expected `MOREFEM_INSTALL_PREFIX` value).

The project is built and ready to use; schemes have been filled with the default values used in tests.


# Tests

Integration tests that run the models and their subsequent conversion to Ensight format are run. Another test check the output is the same as the one recorded previously.

The tests are run through:

```
ninja test
````

or alternatively by:

````
ctest
````

The latter is more powerful and may take some arguments:
    . -R to select a subset of the test. For instance `ctest -R Scalar` will run only tests which name contains Scalar.
    . -V to print on screen the output.
